/**
 * @file
 * JS handler for reading progress bar.
 */
((Drupal, once) => {
  function isElementVisible(el) {
    const style = window.getComputedStyle(el);
    return style.display !== 'none';
  }
  function updateProgressBarValue(progressBar) {
    const value = window.scrollY;
    progressBar.setAttribute('value', value);
  }
  function setupProgressBar(progressBar) {
    /* Set the max scrollable area */
    const winHeight = window.innerHeight;
    const docHeight = document.documentElement.scrollHeight;
    const max = docHeight - winHeight;
    progressBar.setAttribute('max', max);
    const minimumDocumentScreenRatio = progressBar.dataset.minRatio || 2;
    if (docHeight / winHeight > minimumDocumentScreenRatio) {
      progressBar.style.display = '';
    } else {
      progressBar.style.display = 'none';
    }
  }
  function initProgressBar(progressBar) {
    const autoHideDelay = progressBar.dataset.hideDelay || 0;
    // Initial setup.
    setupProgressBar(progressBar);
    // Refresh setup when the page is fully loaded.
    window.addEventListener('load', () => {
      setupProgressBar(progressBar);
    });
    // Refresh setup when the viewport is resized.
    window.addEventListener('resize', () => {
      setupProgressBar(progressBar);
    });
    let autoHideTimer = null;
    document.addEventListener('scroll', () => {
      // Update progress bar if visible only.
      if (isElementVisible(progressBar)) {
        updateProgressBarValue(progressBar);
        if (progressBar.classList.contains('hidden')) {
          progressBar.classList.remove('hidden');
        }
        if (autoHideDelay > 0) {
          if (autoHideTimer) {
            clearTimeout(autoHideTimer);
          }
          autoHideTimer = setTimeout(() => {
            progressBar.classList.add('hidden');
          }, autoHideDelay);
        }
      }
    });
    // Refresh progressbar when a specific container is resized.
    if (progressBar.dataset.containerSelector) {
      const observable = document.querySelector(
        progressBar.dataset.containerSelector,
      );
      if (observable) {
        const resizeObserver = new ResizeObserver((entries) => {
          entries.forEach((entry) => {
            if (entry.target === observable) {
              setupProgressBar(progressBar);
            }
          });
        });
        resizeObserver.observe(observable);
      }
    }
  }

  Drupal.behaviors.reading_progress_bar = {
    attach(context) {
      const progressBars = once('reading_progress_bar', 'progress', context);
      progressBars.forEach((progressBar) => {
        initProgressBar(progressBar);
      });
    },
  };
})(Drupal, once);
