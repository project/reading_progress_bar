# Reading Progress Bar

This will show the reading progress of the user on current page in top bar.
Also known as Reading Position Indicator.

It can be installed on documentation, tutorial, blog sites,
where user can track the progress on top bar as how much content has been read.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/reading_progress_bar).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/reading_progress_bar).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module.
For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
2. Make sure to 'flush all caches'.
3. Check with anonymous user where admin_menu or toolbar is not visible,
   otherwise the reading bar will remain hidden behind the toolbar.


## Maintainers

- Himanshu Pathak - [himanshupathak3](https://www.drupal.org/u/himanshupathak3)
